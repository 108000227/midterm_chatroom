var postsRef = firebase.database().ref('data/public');
var roomsRef = firebase.database().ref('room');
var postsList = document.getElementById('post_list');
var user_email = '';
var cur_room = "public";

function init() {  
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout_btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            logout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    console.log("logout successfully");
                    window.location.href = "signin.html";
                }).catch(function(err) {
                    console.log(err.message);
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    // change to another room
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    var tmp_url = "";

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "" || tmp_url != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var str = post_txt.value;
            for(var i=0; i<str.length; i++) {
                if(str[i] == "<") {
                    var back = str.slice(i+1, str.length);
                    var front = str.slice(0, i);
                    str = front + "&lt" + back;
                    i += 2;
                }
            }

            postsRef.push({
                email: user_email,
                data : str,
                url: tmp_url
            });
            post_txt.value = "";
            tmp_url = "";
        }
    });

    img_btn = document.getElementById('img_btn');
    img_btn.addEventListener('change', function(){
        /// TODO 2: Put the image to storage, and push the image to database's "com_list" node
        ///         1. Get the reference of firebase storage
        ///         2. Upload the image
        ///         3. Get the image file url, the reference of "com_list" and push user email and the image
        const ref = firebase.storage().ref();
        if(ref) {
            const file = img_btn.files[0];
            const name = file.name;
            const metadata = {
                contentType: file.type
            };
            const task = ref.child(name).put(file, metadata);
            post_btn.disabled = true;

            task
            .then(snapshot => snapshot.ref.getDownloadURL())   
            .then((url) => {
                tmp_url = url;        
                img_btn.src = url;
                post_btn.disabled = false;
            })
            .catch(console.error);
        }          
    });

    init_room();
    show_message();
}

function init_room() {
    roomsRef.once('value') 
        .then(function(snapshot) {
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();

                if(childData.email == user_email && childData.name != "") {
                    var button = document.createElement("button");
                    button.id = childData.name;
                    button.type = "button";
                    button.innerHTML = childData.name;                
                    button.className = "btn btn-lg btn-primary btn-block";

                    button.onclick = function() {
                        change_room(button.id);
                    }

                    var tmp = document.getElementById("rooms");
                    tmp.appendChild(button);
                }            
            });  
        })
        .catch(e => console.log(e.message));
}

function create_room() {
    var name = prompt("Enter a room name", "");
    if(name) {
        for(var i=0; i<name.length; i++) {
            if(name[i] == "<") {
                var back = name.slice(i+1, name.length);
                var front = name.slice(0, i);
                name = front + "&lt" + back;
                i += 2;
            }
        }

        var button = document.createElement("button");
        button.id = name;
        button.type = "button";
        button.innerHTML = name;
        button.className = "btn btn-lg btn-primary btn-block";

        button.onclick = function() {
            change_room(button.id);
        }

        var tmp = document.getElementById("rooms");
        tmp.appendChild(button);

        roomsRef.push({
            email: user_email,
            name: name,
            admin: 1
        });
        change_room(name);
    }
}

function change_room(name) { 
    postsRef = firebase.database().ref("data/" + name);
    document.getElementById("room_title").innerHTML = name;
    cur_room = name;
    check_delete();
    check_quit();
    show_message();
}

function show_message() {
    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div>\n";
    // Images parts
    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'></div>";

    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content + str_before_img + childData.url + str_after_img;
                first_count += 1;
            });

            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content + str_before_img + childData.url + str_after_img;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

function add_friends() {
    var new_friend = prompt("Enter an email");
    if(new_friend) {
        roomsRef.push({
            email: new_friend,
            name: cur_room,
            admin: 0
        });
    }
}

function quit() {
    if(confirm("confirm to quit?")) {
        // remove button
        document.getElementById(cur_room).remove();

        // remove info
        roomsRef.once('value') 
            .then(function(snapshot) {
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.email == user_email && childData.name == cur_room) {
                        firebase.database().ref('room/' + childshot.key).remove();
                    }
                });
                change_room("public");
            })
            .catch(e => console.log(e.message));
    } 
}

function delete_room() {
    if(confirm("confirm to delete?")) {
        document.getElementById(cur_room).remove();
        postsRef.remove();
        update_room(cur_room);
        change_room("public");
    }
}

function check_quit() {
    if(cur_room == "public") {
        document.getElementById("add").disabled = true;
        document.getElementById("quit").disabled = true;
    }
    else {
        document.getElementById("add").disabled = false;
        document.getElementById("quit").disabled = false;
    }
}

function check_delete() {
    var flag = false;
    var delete_btn = document.getElementById("delete");
    roomsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childshot) {
                if(!flag) {
                    var childData = childshot.val();          
                    if(childData.name == cur_room && childData.email == user_email && childData.admin == 1) {
                        delete_btn.disabled = false;
                        flag = true;
                    }                
                    else {
                        delete_btn.disabled = true;
                    }
                }                       
            });
        })
        .catch(e => console.log(e.message));
}

function update_room(tar) {
    var isDeleted = [];
    roomsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childshot) {               
                var childData = childshot.val();          
                if(childData.name == tar) {
                    isDeleted.push(childshot.key);
                }                                                            
            });

            for(var i=0; i<isDeleted.length; i++) {
                console.log('room/' + isDeleted[i]);
                firebase.database().ref('room/' + isDeleted[i]).remove();
            }
        })
        .catch(e => console.log(e.message));     
}



window.onload = function() {
    init();
};