# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm-Project-108000227	

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|

# 作品網址：https://midterm-project-108000227.web.app

## Website Detail Description
1. First login with an account.
2. All the users can read and write in chatroom "public".
3. Users can also create a private chatroom and add specific users to join in.
4. Users can quit a chatroom.
5. Only the chatroom owner can delete that chatroom.


# Components Description : 
1. Membership Mechanism : 
    Can register a new account and login with it or just login with Google account.
2. Database read/write : 
    Only the users that has registered can read or write in the chatroom.
3. RWD :
    The components will change their size when squeezing the screen.
4. Topic Key Functions:
    Every user can create a new chatroom and add another specific user to join so that only the users registered in this chatroom can see or write in this private chatroom.
5. Sign Up/In with Google:
    Users can login with their Google acconts.
6. Deal with messages when sending html code:
    Here I change "<" with "&lt" when storing in the firebase to deal with the HTML tag problem.

# Other Functions Description : 
1. Quit : 
    Users can quit a chatroom so the the specific chatroom will no longer shown.
2. Delete : 
    Only the owner of a specific chatroom can delete it, and this chatroom and messages will be deleted.
3. Upload :
    Users can upload image files with comments.

